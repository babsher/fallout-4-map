Meteor.publish('points', function () {
  if (this.userId) {
    var query = {},
      proj = {
        userId: 1,
        date: 1,
        likeCount: 1,
        spamCount: 1,
        marker: 1
      };
    proj['like.' + this.userId] = 1;
    query['spam.' + this.userId] = {$exists: false};
    return Points.find(query, proj);
  } else {
    return Points.find();
  }
});

Meteor.publish('goodPoints', function (min) {
  return Points.find({votes: {'gt': min}});
});

Meteor.publish('userPoints', function () {
  return Points.find({
    user: this.userId
  })
});

Meteor.methods({
  addPoint: function (marker) {
    if (Meteor.userId()) {
      var point = {
        userId: Meteor.userId(),
        date: new Date(),
        like: {},
        likeCount: 0,
        spam: {},
        spamCount: 0,
        marker: marker
      };
      return Points.insert(point);
    } else {
      return false;
    }
  },
  spam: function (point) {
    if (Meteor.userId()) {
      var s = {};
      s["spam."+Meteor.userId()] = true;
      var update = {
        $set: s,
        $inc: {spamCount: 1}
      };
      return Points.update({_id: point}, update);
    } else {
      return false;
    }
  },
  like: function (point) {
    if (Meteor.userId()) {
      var s = {};
      s["like."+Meteor.userId()] = true;
      var update = {
        $set: s,
        $inc: {likeCount: 1}
      };
      return Points.update({_id: point}, update);
    } else {
      return false;
    }
  }
});