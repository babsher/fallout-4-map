ServiceConfiguration.configurations.upsert(
  { service: "facebook" },
  {
    $set: {
      appId: Meteor.settings.private.facebook.clientId,
      secret: Meteor.settings.private.facebook.secret
    }
  }
);

ServiceConfiguration.configurations.upsert(
  { service: "google" },
  {
    $set: {
      clientId: Meteor.settings.private.google.clientId,
      secret: Meteor.settings.private.google.secret
    }
  }
);

ServiceConfiguration.configurations.upsert(
  { service: "twitter" },
  {
    $set: {
      consumerKey: Meteor.settings.private.twitter.clientId,
      appId: Meteor.settings.private.twitter.clientId,
      clientId: Meteor.settings.private.twitter.clientId,
      secret: Meteor.settings.private.twitter.secret
    }
  }
);