'use strict';

var map;

function buildMap() {
  var margin = 1.5;
  var width = 2048,
    height = 2048;

  var proj = L.Projection.LonLat;
  proj.bounds = L.bounds([0, 0], [255, 255]);
  console.log("Bounds", proj.bounds);

  var crs = L.CRS.Simple;
  crs.infinite = false;
  crs.projection = proj;
  L.Icon.Default.imagePath = 'packages/bevanhunt_leaflet/images';

  map = L.map('map', {
    minZoom: 0,
    maxZoom: 7,
    crs: crs,
    zoomControl: false,
    attributionControl: false
  });

  L.tileLayer('/map/{z}/{x}/{y}.png', {
    id: 'fallout4map',
    tms: true,
    reuseTiles: true
  }).addTo(map);

  // Restrict to bounds
  var max = new L.LatLngBounds([(-10) * margin, (-10) * margin], [255 * margin, 255 * margin]);
  // console.log('max', max);
  map.setMaxBounds(max);
  // Fit bounds
  // map.fitBounds(L.LatLngBounds([0, 0], [255, 255]));
  map.setView([128, 128], 3);

  //map.on('click', function (e) {
  //  console.log(e.containerPoint, e.layerPoint, e.latlng, e);
  //});

  return map;
}

angular.module('fallout4mapApp')
  .controller('MainCtrl', function ($scope, $rootScope, $meteor, $mdToast) {

    $scope.allPoints = [];
    $meteor.subscribe('points').then(function(handle) {
      $scope.allPoints = $meteor.collection(Points);
    });

    function updatePoints() {
      if(!map) {
        window.setTimeout(updatePoints, 100);
      } else {
        $scope.allPoints.forEach(function (point) {
          var latlng = L.latLng(point.marker.latlng.lat, point.marker.latlng.lng);
          var p = L.marker(latlng).addTo(map);
          p.data = point.marker;
          p.liked = point.like[Meteor.userId()] === true;
          p.id = point._id;
          p.on('click', pointListener(p))
        });
      }
    }

    $scope.$watch('allPoints', updatePoints);

    function checkNew() {
      if ($scope.point != null) {
        if($scope.point.isNew) {
          map.removeLayer($scope.point);
        }
      }
    }

    function pointListener(point) {
      return function() {
        console.log('clicked', point);
        checkNew();
        $scope.point = point;
        $scope.$digest();
      };
    }

    $scope.navOpen = false;
    $scope.point = null;

    function addPoint(latlng) {
      checkNew();

      var point = L.marker(latlng, {
        draggable: true
      });
      point.isNew = true;
      point.data = {};
      $scope.point = point;
      point.addTo(map);
      $scope.$digest();

      $scope.$emit('point:new', point);
    }

    $scope.cancel = function() {
      map.removeLayer($scope.point);
      $scope.point = null;
    };

    $scope.save = function() {
      if($rootScope.currentUser) {
        if ($scope.point) {
          var point = $scope.point.data;
          point.latlng = $scope.point.getLatLng();
          $meteor.call('addPoint', point);
          $scope.point.isNew = false;
          $scope.point.dragging.disable();
          $scope.point.on('click', pointListener($scope.point));
        }
      } else {
        $mdToast.show($mdToast.simple().content('Must be logged in to save locations.'));
      }
    };

    $scope.like = function() {
      if($scope.point && $scope.point.id) {
        $scope.point.liked = true;
        $mdToast.show($mdToast.simple().content('Thanks!'));
        $meteor.call('like', $scope.point.id);
      }
    };

    $scope.spam = function() {
      console.log($scope.point);
      if($scope.point && $scope.point.id) {
        $meteor.call('spam', $scope.point.id);
        map.removeLayer($scope.point);
        $scope.point = null;
        $mdToast.show($mdToast.simple().content('Thanks!'));
      }
    };

    window.setTimeout(function() {
      map = buildMap();
      map.on('contextmenu', function (e) {
        addPoint(e.latlng);
      });
    }, 100);

  });