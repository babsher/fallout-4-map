'use strict'

angular.module('fallout4mapApp')
.config(function($mdThemingProvider) {
  $mdThemingProvider.theme('default');
});