'use strict';

angular.module('fallout4mapApp')
  .directive('toolbar', function () {

    function toast($mdToast, msg) {
      $mdToast.show($mdToast.simple().content(msg));
    }

    function DialogController($scope, $mdDialog, $mdToast, $meteor) {
      $scope.facebook = function () {
        Meteor.loginWithFacebook({}, function (err) {
          if (err) {
            toast($mdToast, 'Facebook login failed');
          } else {
            toast($mdToast, 'Success!');
            $mdDialog.hide();
          }
        });
      };

      $scope.google = function () {
        $meteor.loginWithGoogle({}).then(function (err) {
          $mdDialog.hide();
          if (err) {
            toast($mdToast, 'Google login failed');
          } else {
            toast($mdToast, 'Success!');
            $mdDialog.hide();
          }
        });
      };

      $scope.twitter = function () {
        Meteor.loginWithTwitter({}, function (err) {
          if (err) {
            toast($mdToast, 'Twitter login failed');
          } else {
            toast($mdToast, 'Success!');
            $mdDialog.hide();
          }
        });
      };
    }

    return {
      restrict: 'AE',
      templateUrl: 'client/components/toolbar/toolbar.view.ng.html',
      replace: true,
      controller: function ($scope, $rootScope, $mdToast, $meteor, $mdDialog) {
        $scope.openLogin = function () {
          $mdDialog.show({
            clickOutsideToClose: true,
            templateUrl: 'client/components/toolbar/login-dialog.view.ng.html',
            controller: DialogController
          });
        };

        $scope.logout = function () {
          $meteor.logout().then(function() {
            toast($mdToast, 'Logged Out');
          });
        };
        $scope.isLoggedIn = $meteor.currentUser != null;

        $scope.help = function() {
          var alert = $mdDialog.alert({
            title: 'Help!',
            content: 'Login to add points. Right click to create a new point. Drag the ' +
            'point to where you want it. Then fill out the info.',
            ok: 'Close'
          });
          $mdDialog.show(alert)
            .finally(function() {
            alert = undefined;
          });
        }
      }
    };
  });