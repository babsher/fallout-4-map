angular.module('fallout4mapApp', [
  'angular-meteor',
  'ui.router',
  'ngMaterial'
]);

onReady = function() {
  angular.bootstrap(document, ['fallout4mapApp']);
};
  
if(Meteor.isCordova) {
  angular.element(document).on('deviceready', onReady);
} else {
  angular.element(document).ready(onReady);
}